import heapq

class Vertex:
    def __init__(self, frequency, character=None, left_child=None, right_child=None, depth=None) -> None:
        self.left_child: Vertex = None
        self.right_child: Vertex = None
        self.frequency: int = frequency
        self.character: str = character
        self.code: str = ''
        self.depth = depth


    def __str__(self) -> str:
        return f"char: {self.character}, freq: {self.frequency}, code: {self.code}, depth: {self.depth}"

    def __lt__(self, other) -> bool:
        return self.frequency < other.frequency

    def __gt__(self, other) -> bool:
        return self.frequency > other.frequency

    def __eq__(self, other) -> bool:
        return self.frequency == other.frequency


    def add_children(self, left, right) -> bool:
        if not isinstance(left, Vertex) or not isinstance(right, Vertex):
            return False
        self.left_child = left
        self.right_child = right
        return True



class HuffmanTree:
    def __init__(self, character_frequency_dict) -> None:
        self.character_frequency_dict = character_frequency_dict # get frequency given character
        self.vertices = self.generate_vertices() # all vertices inside the tree
        self.root_vertex = self.generate_tree(self.vertices)
        self.fill_vertices_array(self.root_vertex)


    def generate_vertices(self):
        vertices = []
        for character in self.character_frequency_dict:
            frequency = self.character_frequency_dict[character]
            vertices.append(Vertex(frequency, character))
        return vertices


    def generate_tree(self, vertices):
        heapq.heapify(vertices)
        depth = 0
        while (len(vertices) > 1):
            left_vertex: Vertex = heapq.heappop(vertices)
            left_vertex.depth = depth+1
            right_vertex: Vertex = heapq.heappop(vertices)
            right_vertex.depth = depth+1
            parent_vertex = Vertex(left_vertex.frequency + right_vertex.frequency, None, left_vertex, right_vertex, depth=depth)
            parent_vertex.add_children(left_vertex, right_vertex)
            heapq.heappush(vertices, parent_vertex)
            depth += 1
        root_vertex = heapq.heappop(vertices)
        self.encode_vertices(root_vertex)
        return root_vertex


    def encode_vertices(self, vertex: Vertex, code=''):
        if (isinstance(vertex.left_child, Vertex)):
            self.encode_vertices(vertex.left_child, code+'0')
        if (isinstance(vertex.right_child, Vertex)):
            self.encode_vertices(vertex.right_child, code+'1')
        if (isinstance(vertex.character, str)):
            vertex.code = code


    def fill_vertices_array(self, vertex: Vertex):
        if (isinstance(vertex.left_child, Vertex)):
            self.fill_vertices_array(vertex.left_child)
        if (isinstance(vertex.right_child, Vertex)):
            self.fill_vertices_array(vertex.right_child)
        if (isinstance(vertex.character, str)):
            heapq.heappush(self.vertices, vertex)
        

    def print_vertices(self):
        print("Normal huffman tree:")
        for vertex in self.vertices:
            print(vertex)


    def print_average_code_length(self):
        code_lengths = [len(vertex.code) for vertex in self.vertices]
        average_code_length = sum(code_lengths) / len(self.vertices)
        print(f"average code length: {average_code_length}")


    def decompress_character(self, bitstring: str, vertex: Vertex) -> str:
        if not bitstring:
            if vertex.character: return vertex.character
            else: return "No character found"

        bit = bitstring[0]
        if bit == '0': child = vertex.left_child
        elif bit == '1': child = vertex.right_child
        else: return f"Invalid bit: {bit}"

        return self.decompress_character(bitstring[1:], child)



def main():
    character_frequency_dict = {
        'E': 50, 'T': 43, 'A': 38, 'H': 13, 'S': 12, 'O': 31, 'I': 17, 'N': 20, 'R': 10, 'L': 4
    }
    tree = HuffmanTree(character_frequency_dict)
    tree.print_vertices()
    tree.print_average_code_length()
    print("1011 ->", tree.decompress_character("1011", tree.root_vertex))



if __name__ == "__main__":
    main()