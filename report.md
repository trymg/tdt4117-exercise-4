# TDT4117 Information Retrieval - Autumn 2021
# Assignment 4

# Task 1: Text Compression
## 1
Normal huffman codes from [task_1.py](task_1.py):
```bash
char, freq, code
L, 4, 10100
I, 17, 1011
R, 10, 10101
N, 20, 000
O, 31, 100
H, 13, 0011
S, 12, 0010
E, 50, 01
A, 38, 110
T, 43, 111
```

Normal huffman tree:
![](huffman_tree.png)

Converting to canonical form:

Sorted on bit lengths, sorted lexicographically on equal bit lengths:
char, bitlength
['E', 2]
['A', 3]
['N', 3]
['O', 3]
['T', 3]
['H', 4]
['I', 4]
['S', 4]
['L', 5]
['R', 5]

Canonical huffman codes:
['E', '00', 2],
['A', '010', 3],
['N', '011', 3],
['O', '100', 3],
['T', '101', 3],
['H', '1100', 4],
['I', '1101', 4],
['S', '1110', 4],
['L', '11110', 5],
['R', '11111', 5]


Canonical huffman tree:
![](canonical_huffman_tree.png)

Average length = (2+3+3+3+3+4+4+4+5+5) / 10 = 3.6 bits
The average length of the code is 3.6 bits.

The average length of code if frequency was equal:

Leaf nodes would be spread out as symmetrically (flat) as possible:
![](2021-11-08-11-06-03.png)
Average length = (3+3+3+3+3+3+4+4+4+4) / 10 = 3.4 bits


## 2
00010001000100011
000 10 0010 0010 0011
H   A   L   L   O

00111011010
0011 10 11 010
O   A   T   U

# Task 2 : Index Analysis Using Lucene
## 1
Lucene is an open-source Java library for text indexing and searching. It provides developers with powerful features that can be used in almost any application requiring full-text search. It is easy to use, has good text indexing-performance (over 150GB/hour on modern hardware), as well as powerful and efficient search algorithms. Lucene was originally written by Doug Cutting in 1999, and has since been supported by the Apache Software Foundation. 


## 2
![](first_indexing.png)

The indexing process consists of first adding the documents to IndexWriter. IndexWriter then uses Analyzer/StandardAnalyzer to decide whether to create, open or edit indexes, and then store them in a directory. 


## 3
"God":

![](first_god_search.png)

"circumstances":

![](circumstances_search.png)

"claims of duty":

![](claims_of_duty_search.png)

All documents containing any of the words in the search query are returned (OR). This can be seen by the fact that Text1.txt is returned for the query «claims of duty», but it does not include «claims» or «duty», only «of». 


## 4
Indexing:

![](second_indexing.png)

Search query «Norwegian University Science Technology»:

![](ntnu_search.png)

Search query "god": 

![](second_god_search.png)